# Import the model and other libraries
import os
import tempfile
import argparse
import time
import grpc
import json, requests
import numpy as np
from PIL import Image
import tensorflow as tf
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc

tmpdir = tempfile.mkdtemp()

def load_labels(filename):
  with open(filename, 'r') as f:
    return [line.strip() for line in f.readlines()]

def get_predictions(pic_file_path, server, model_name, protocol):
  SERVER_URL = 'http://' + server + ':8501/v1/models/' + model_name +':predict'
  SERVER_URL_grpc = server + ':8500'
  total_time = 0
  label_file = '/hotdog_image_classification_app/class_labels.txt'  
  labels = load_labels(label_file)

  #REST Call
  if protocol == 'rest':
    # Load image and preprocess it
    # Compose a JOSN Predict request (send the image tensor).
    jpeg_rgb = Image.open(pic_file_path)
    # Normalize and batchify the image
    jpeg_rgb = np.expand_dims(np.array(jpeg_rgb) / 255.0, 0).tolist()
    predict_request = json.dumps({'instances': jpeg_rgb})
    headers = {"content-type": "application/json"}
    #request and report average latency.
    
    response = requests.post(SERVER_URL, data=predict_request, headers=headers)
    response.raise_for_status()
    total_time += response.elapsed.total_seconds()
    prediction = response.json()['predictions'][0]

  else: #gRPC
    image = Image.open(pic_file_path)
    image = np.array(image) / 255.0
    image = np.expand_dims(image, 0)
    image = image.astype(np.float32)
    GRPC_MAX_RECEIVE_MESSAGE_LENGTH = 4096 * 4096 * 3
    channel = grpc.insecure_channel(SERVER_URL_grpc, options=[('grpc.max_receive_message_length', GRPC_MAX_RECEIVE_MESSAGE_LENGTH)])
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
    grpc_request = predict_pb2.PredictRequest()
    grpc_request.model_spec.name = 'new_model'
    grpc_request.model_spec.signature_name = 'serving_default'

    start = time.time()
    grpc_request.inputs['input_1'].CopyFrom(tf.make_tensor_proto(image))
    prediction = stub.Predict(grpc_request,5000)
    end = time.time()
    total_time = end-start
    prediction = prediction.outputs['dense'].float_val

  print('Prediction class: {}, latency: {} ms'.format(
    labels[np.argmax(prediction)], (total_time * 1000)))
    
  if labels[np.argmax(prediction)] == "hot_dog":
    pred = "Hot Dog"
  else:
    pred = "Not Hot Dog"
  
  complete_pred = (pred,total_time)
  return complete_pred
