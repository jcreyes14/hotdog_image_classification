# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository follows the Image Recognition blog post that is found [here](get link).



### Creating a Custom TensorFlow Model ###

First, we need to configure our developer machine to train our model. 
We need to install the latest Docker version, Python, TensorFlow, TKG CLI, Kubernetes CLI, Kompose. 
Don't forget to log into your Docker Hub account if you plan to store the containers there.
TensorFlow provides excellent tools that utilize pre-trained models to create a custom one. 
The TensorFlow_hub library contains the make_image_classifier tool, which lets you build and train a TensorFlow model for image classification from the command line, requiring no coding.

$ pip install "TensorFlow~=2.0"
$ pip install "TensorFlow-hub[make_image_classifier]~=0.6"
We also need the dataset we are going to train our model before executing the make_image_classifier command.
There are plenty of available datasets to download, and you can certainly also search and save individual images.
Since I am a huge fan of the Silicon Valley series, I will use a hotdog, not a hotdog dataset. [link]
This dataset contains 250 images in each set (hotdog images, not hotdog images). 
Save the dataset in the developer machine.

Now here is an example command of the make image classifier:

$ make_image_classifier \
  --image_dir my_image_dir \
  --tfhub_module https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4 \
  --image_size 224 \
  --saved_model_dir my_dir/new_model \
  --labels_output_file class_labels.txt \
  --tflite_output_file new_mobile_model.tflite \
  --summaries_dir my_log_dir

You can view the description of all the available flags for this tool on the TensorFlow git repository. ¬¬¬¬¬¬
I want to note that when choosing the tfhub module of your likings, make sure you select one optimized for image classification as it will yield higher accuracy. 
You can also play around with the epochs flag to get more accurate results, but after a certain number, the accuracy doesn't go much higher, especially with a low number of images to train.
For my model, I used the inception v3 tfhub model (https://tfhub.dev/google/tf2-preview/inception_v3/classification/4) as it gave me the best accuracy rate with the number of images I fed it. 

 	
The amount of time the model takes to be produced depends on several factors like the VM specs, the number of epochs, and the number of images you are feeding it. For perspective, my developer machine took about three minutes to finish training the model. 

We can use the label_image script to test the custom model with the model now created before moving it to a container.
This script utilizes the tflite file created in the previous step (the flag must've been enabled in the make image classifier command).

Here is an example command:

$ python3 label_image.py --input_mean 0 --input_std 255 --model_file new_mobile_model.tflite --label_file class_labels.txt --image ./dir_to_image/image.jpg

This script needs a few inline arguments: tflite model that is simpler to call, and you don't have to worry about the REST and gRPC protocols, label_class text file created from the make_image_classifier script, and the image that will be labeled.

Let's run the command. 

It works! After confirming that the model is labeling our image correctly, we can put the model on the TensorFlow serving container.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact